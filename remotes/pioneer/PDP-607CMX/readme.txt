The manual for the 607CMX does not mention the model number of the remote that
is to be used with the display.  They only provide an image of the remote in
the manual.  That image is in 607CMX.png.

Checking Pioneer's support site, I found that the remote is AXD1528.  Image in
AXD1528.png.

In searching the net, I found a site that claims the AXD1534 is a good
substitute.  The exceptions are: AXD1534 Supports All Features Except NO POINT
ZOOM - FREEZE - SPLIT - SUB INPUT - SWAP - PIP SHIFT - ID NO. SET - Or CLEAR.
Image in AXD1534.png.

There is no 1534 in the LIRC database.  However, there is a AXD-1531.  Image
in AXD1531.png.

The LIRC remote database has an entry for the AXD-1531.
    http://lirc.sourceforge.net/remotes/pioneer/

The 1531 might be a good bet for initial testing without irrecord, since I
won't have an IR receiver until tomorrow.

