include common.mk

.phony: transfer lircd.conf documentation

# static IP of Pi
IP=192.168.1.100
USER=pi

all: transfer

# xfer:	lircd.conf
# 	rsync -avz --exclude .git/ -e ssh ../remote/              $(USER)@$(IP):remote
# 	rsync -avz                 -e ssh remotes/lircd.conf      $(USER)@$(IP):/etc/lirc/lircd.conf

transfer: lircd.conf
	rsync -avz --exclude .git/ --exclude setup/ -e ssh ../remote/  $(USER)@$(IP):remote

lircd.conf:
	cd remotes; make lircd.conf

documentation:
	cd docs; make

Readme.pdf: Readme.md

pdf: Readme.pdf
	cd docs; make



