'''
TODO: - /device/group/command is needlessly complex, group is never
        used.  Revise to /device/command, which will simplify code.
      - Keep track of states and use to control icon presentation.
      - Add preconditions to commands.  Perhaps 'device::state'.
      - convert to using Fabric to deploy
'''

from pprint import pprint as pp

from flask import Flask, request, session, g, current_app
from flask import redirect, url_for, abort, render_template, flash


from commands import Controller, Device, development_system
from channel_groups import ChannelGroups


def debug():
    '''
    Use this anywhere you want to trigger the debugger.
    See: http://flask.pocoo.org/snippets/21/
    '''
    msg = 'You were taken here by a call to debug().'
    assert current_app.debug == False, msg


app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('REMOTE_SETTINGS', silent=True)
app.jinja_env.line_statement_prefix = '%'


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', ChannelGroups=ChannelGroups)


@app.route('/cbl/<function>/<state>')
def cbl(function, state):
    path = aud.func_globals['request'].path
    history = Device.get('cbl').send(path)
    pp(history)
    return ''


@app.route('/tv/<function>/<state>')
def tv(function, state):
    path = aud.func_globals['request'].path
    history = Device.get('tv').send(path)
    pp(history)
    return ''


@app.route('/aud/<function>/<state>')
def aud(function, state):
    path = aud.func_globals['request'].path
    history = Device.get('aud').send(path)
    pp(history)
    return ''


@app.route('/sys/<function>/<state>')
def system(function, state):
    path = aud.func_globals['request'].path
    history = Device.get('sys').send(path)
    pp(history)
    return ''


if __name__ == '__main__':

    from pprint import pprint as pp
    import sys

    controller = Controller()

    port = 5000
    app.run('0.0.0.0', port=port, debug=True)
