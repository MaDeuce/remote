from __future__ import with_statement

import os
import re
import StringIO

from pprint import pprint as pp

import cuisine
import fabric.contrib.files as fcf
import fabric.operations as fo
import fabric.utils as fu
import fabric.colors as fc

from fabric.api import env, settings, run, put
from fabric.contrib.console import confirm


# Default Raspbian id/passwd
env.user  = 'pi'
env.password = 'raspberry'

# Full path to the 'remote' installation location on the Pi
env.remote_dir = '/home/pi/remote'


def ps():
    run('ps agxuw')


def msg(message):
    fu.puts(fc.white(message))


def reboot():
    msg('rebooting')
    msg("'Fatal error' immediately after this message is OK.")
    fo.sudo('reboot')


def expandfs():
    fo.sudo('raspi-config --expand-rootfs')
    reboot()


def setup_projdir():
    # this is idempotent
    cuisine.dir_ensure(env.remote_dir, mode='0755')
    set_timezone()
    update_profile()


def change_hostname(newname='remote'):
    # this is idempotent, albeit with reboot
    config_file = '/etc/hosts'
    fcf.sed(config_file, 'raspberrypi', newname, use_sudo=True)
    config_file = '/etc/hostname'
    fcf.sed(config_file, 'raspberrypi', newname, use_sudo=True)
    fo.sudo('/etc/init.d/hostname.sh')
    reboot()


def configure_wifi():
    # this is idempotent, albeit with restarting wlan
    ssid = env.ssid
    psk = env.psk
    wifi_conf = ('network={{\n'
                 '   ssid="{ssid}"\n'
                 '   psk="{psk}"\n'
                 '}}\n').format(**locals())
    config_file = '/etc/wpa_supplicant/wpa_supplicant.conf'
    ensure_original_backup(config_file)
    fcf.append(config_file, wifi_conf, use_sudo=True)
    fo.sudo('ifdown wlan0')
    fo.sudo('ifup wlan0')


def configure_ssh():
    # this is idempotent
    # First configure authorized_keys
    cuisine.dir_ensure('~/.ssh', mode='0700')
    # *local* id_rsa.pub to *remote* authorized_keys
    put('~/.ssh/id_rsa.pub', '~/.ssh/authorized_keys', mode='0600')

    # Then disable password logins.  This is brittle; it expects
    # exactly one space between kw and value.
    config_file = '/etc/ssh/sshd_config'
    keyword = 'PasswordAuthentication'
    enabled = '%s yes' % keyword
    disabled = '%s no' % keyword
    target_re = '(%s)|(%s)' % (enabled, disabled)
    fcf.uncomment(config_file, target_re, use_sudo=True)
    fcf.sed(config_file, enabled, disabled, use_sudo=True)
    fo.sudo('/etc/init.d/ssh restart')


def update_software():
    # this is idempotent
    fo.sudo('apt-get update')
    fo.sudo('apt-get upgrade -y')
    cuisine.select_package('apt')
    for pkg in ['emacs', 'lirc', 'lsof', 'libnss-mdns']:
        cuisine.package_ensure(pkg)
    for pkg in ['flask']:
        fo.sudo('pip install %s' % pkg)


def configure_lirc():
    # this is idempotent
    config_file = '/boot/config.txt'
    line = 'dtoverlay=lirc-rpi,gpio_in_pin=22,gpio_out_pin=23'
    fcf.append(config_file, line, use_sudo=True)

    config_file = '/etc/lirc/hardware.conf'
    ensure_original_backup(config_file)
    fcf.sed(config_file, 'DRIVER="UNCONFIGURED"', 'DRIVER="true"',use_sudo=True)
    fcf.sed(config_file, 'DEVICE=""',  'DEVICE="/dev/lirc0"', use_sudo=True)
    fcf.sed(config_file, 'MODULES=""', 'MODULES="lirc_rpi"',  use_sudo=True)

    config_file_local = './remotes/lircd.conf'
    config_file_remote = '/etc/lirc/lircd.conf'
    put(config_file_local, config_file_remote, use_sudo=True)
    cuisine.file_ensure(config_file_remote, mode=0660, owner='pi', group='pi')

    fo.sudo('/etc/init.d/lirc stop')
    fo.sudo('/etc/init.d/lirc start')

    # start remote at boot time without systemd interface
    # rc_local = '/etc/rc.local'
    # fcf.append(rc_local, 'python /home/pi/remote/remote.py &')

    return


def s2f(string):
    '''
    Convert a string to a file-like object that can later be read like
    a file.
    '''
    flo = StringIO.StringIO()
    flo.write(string)
    flo.seek(0)
    return flo


def set_timezone(tz='US/Central'):
    '''
    Set the timezone.

    Hint: you can find your timezone via:
        sudo dpkg-reconfigure tzdata
    '''

    fo.sudo('echo "%s" > /etc/timezone' % tz)
    fo.sudo('dpkg-reconfigure -f noninteractive tzdata')
    

def update_profile():
    '''
    Turn off those god-awful colorized prompts and listings.
    '''
    rcfile = '~/.bashrc'
    fcf.comment(rcfile, 'force_color_prompt=yes')
    fcf.append(rcfile, "alias ls='ls -CF'")


def configure_boot():
    '''
    Configure systemd to do a few things for us each time the system
    is booted.
    '''

    start_at_boot()
    forward_port_80()


def start_at_boot():
    '''
    Configure systemd to start 'remote' at boot time.
    
    Good systemd reference:
    https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/\
          System_Administrators_Guide/chap-Managing_Services_with_systemd.html

    NOTE: For some reason, the logging to remote.log that is attempted
          in remote.service doesn't happen.  Rather, all log output
          is placed in /var/log/daemon.log.
    '''

    unit_file = '/lib/systemd/system/remote.service'
    log_dir = os.path.join(env.remote_dir, 'var')
    cuisine.dir_ensure(log_dir, mode='0755')

    config = '''
        [Unit]
        Description=My Script Service
        After=multi-user.target

        [Service]
        Type=idle
        User=pi
        Group=pi
        ExecStart=/usr/bin/python -u {rd}/remote.py > {ld}/remote.log 2>&1

        [Install]
        WantedBy=multi-user.target
    '''.format(rd=env.remote_dir, ld=log_dir)
    config = re.sub('\n[ \t]+', lambda x: '\n', config)
    put(s2f(config), unit_file, use_sudo=True, mode=0644)
    fo.sudo('systemctl daemon-reload')
    fo.sudo('systemctl enable remote.service')
    fo.sudo('systemctl start remote.service')
    

def configure_port_80():
    '''
    Flask is running its server on its standard port of 5000.  It
    could run on port 80 if run as root, but that's a bad idea.
    Rather, forward all traffic on port 80 to port 5000.  That way
    'remote' can run as user 'pi', which is safer.
    '''

    iptables_rules = '/home/pi/remote/iptables.rules'
    configure_iptables_initial(iptables_rules)
    configure_iptables_at_boot(iptables_rules)


def configure_iptables_initial(rulesfile):
    '''
    Perform initial configuration of iptables and save resulting rules.

    This initial config should only be done once.  Subsequent to this,
    at every boot, the rules file this function creates will be used to
    reconfigure iptables.
    '''

    # configure iptable to forward as desired
    fo.sudo('iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 5000')
    # save the configuration for use at subsequent boots
    fo.sudo('iptables-save > %s' % rulesfile)


def configure_iptables_at_boot(rulesfile):
    '''
    Configure systemd to use iptables config at each boot.

    Creates a systemd service called 'forward5000', which will be run
    at boot time to load the iptables configuration created by
    configure_iptables_initial().
    '''

    unit_file = '/lib/systemd/system/forward5000.service'
    config = '''
        [Unit]
        Description=Packet Filtering Framework

        [Service]
        Type=oneshot
        ExecStart=/sbin/iptables-restore  {rulesfile}
        ExecReload=/sbin/iptables-restore {rulesfile}
        # To be correct, the configuration should be flushed here,
        # but for our use, nothing is gained by the extra effort.
        # ExecStop=/usr/lib/systemd/scripts/iptables-flush
        RemainAfterExit=yes

        [Install]
        WantedBy=multi-user.target
    '''.format(rulesfile=rulesfile)
    config = re.sub('\n[ \t]+', lambda x: '\n', config)
    put(s2f(config), unit_file, use_sudo=True, mode=0644)
    fo.sudo('systemctl daemon-reload')
    fo.sudo('systemctl enable forward5000.service')
    fo.sudo('systemctl start  forward5000.service')


def unforward_port_80():
    '''
    Since 'remote' is installed and run on a virgin Raspbian, we know
    that the first iptables rule will be the one we added by
    forward_port_80().  Therefore, we'll just drop rule 1.

    Otherwise, one would have to run
    'iptables -t nat --line-numbers -n -L' to find the rule number to
    remove. 
    '''
    fo.sudo('iptables -t nat -D PREROUTING 1')


def status():
    fo.sudo('systemctl status remote.service')
    fo.sudo('systemctl status forward5000.service')


def dmesg():
    run('dmesg')


def daemonlog():
    run('cat /var/log/daemon.log')
    

def ensure_original_backup(file):
    config_file = file
    config_file_orig = file + '.orig'
    if not fcf.exists(config_file_orig):
        fo.sudo('cp %s %s' % (config_file, config_file_orig))


def iwlist():
    fo.sudo('iwlist wlan0 scan')

