# Installing `remote` on a Raspberry Pi 3

These are some extremely brief instructions on how to install `remote` on a
Pi.  They are really intended more as 'notes' than detailed instructions for a
third party to follow.

These instructions assume a RPi3, a common router shared by the PC used to
perform the installation and the RPi, with the RPi initially hardwired to the
router.  It is possible to configure the SD card to allow setup over WiFi, but
that is not presented here.

It is assumed the PC used for the installation is running OS-X.

All paths shown in this guide assume that `$R` is the root directory of this
project.

Fully automated install process - never a need to login to the Pi to get
`remote` up and running.

## Local Setup

You'll need a few tools on the PC that you will be using to configure the Pi.
This assumes you have a python, you've create a `virtualenv` named `remote`
and that `remote` is currently selected.

(@) Install the python packages that will be used:

	```bash
	$ pip install -r $R/requirements_local.txt
	```

	If you wish to build the documentation, you will need to have `pandoc`
    installed via `ports`, `homebrew`, or whatever.

## Basic Pi Setup

### Install Raspbian

This is largely based on these [instructions](
https://www.raspberrypi.org/documentation/installation/installing-images/mac.md
).

The first step is to download a Raspbian image and install it on a SD card.  I
used a 32GB Samsung SD.

(@) cd to the `$R/setup`

(@) Download the Raspbian image.  The location and version of the image is
    specified in `Makefile`.  The image is only downloaded if it is not
    already present in `./setup`.

    ```bash
    $ make getimg
    ```
    
(@) Insert the card into the reader.

    Examine the output from `diskutil list` to determine the device assigned
    to the SD card.  Be careful.  These instructions will assume the device is
    `/dev/disk2`. 

	Run `diskutil list`.  Examine the output.  If it shows that the SD card
    was automounted, unmount it using `diskutil unmountDisk /dev/disk2`.


(@) Install the image on the SD card.  Be extremely careful in specifying the
    device name on which to install the image.  If you use the wrong one, you
    could destroy something.

	*IMPORTANT: the raw device must be used in this step.  If the device in
     the previous step was `/dev/disk2`, the raw device, `/dev/rdisk2`, will
     be used in this step.*

	```bash
	$ make writeimg
	```

	The `Makefile` will prompt you for the device associated with the SD card.
	*Supply the raw device (e.g., `/dev/rdisk2`) here.*

	When the process is complete, the image will be installed on the SD card.

	If the SD was automounted, unmount it via: `diskutil unmountDisk
    /dev/disk2`.

(@) Starting in late 2016, raspbian began turning off SSH by default.  If a
    file named `ssh` is present in `/boot/` at boot time, SSH will be
    enabled.  There is a rule in the `Makefile` to do this:

    ```bash
    $ make ssh_enable
	```

### Configure Raspbian

After completing the previous step, The Raspbian image will be on the SD card.

(@) Insert the SD card into the Pi.

(@) For this configuration procedure, both the Pi and the PC that is being
    used to configure the Pi must be connected to the same router.  The Pi must be
    hardwired to the router (e.g, via Cat5).

(@) Connect the Pi to power.  Wait a few minutes for it to boot.

(@)	Connect to the admin page of the router.  Look for the IP address of the
    Pi.  The default device name will be `raspberrypi`.  There are a number of
    other ways to get the IP address.  These instructions assume that
	`IP=192.168.1.7`.

(@) Confirm that you can login to the Pi using `ssh`.  The username is `pi`
    and the password is `raspberry`. 

	```bash
	$ ssh pi@$IP
	```

    If the local `ssh` prompts you, accept the authenticity of the host.
    Despite the warning, you do not need to change the password, due to steps
    taken further in this note.

	NOTE: Generally speaking, you don't want to leave both SSH and password
	logins enabled.  The `configure_ssh` step below will install your personal
	SSH key, which will be used for future access;  it will also disable
	password-based logins.  These are both good steps towards securing the
	Pi.

	You can either remain logged in logout.  Being logged in is not necessary
    for the remainder of the configuration.

### Continue to Configure Raspbian using Fabric

(@) Confirm that Fabric is installed and working on the local system.  Do this
    by using Fabric to do a `ps` on the remote machine.

	```bash
	$ cd $R           # all commands in this section are invoked from here
    $ fab -H $IP ps
	```

(@) Expand the root filesystem to fill the entire space available on the SD
    card:

	```bash
	$ fab -H $IP expandfs
	```

	The Pi will reboot.

(@) Wait for the machine to reboot. Change the hostname to `remote`:

	```bash
	$ fab -H $IP change_hostname
	```

	The Pi will reboot.

	NOTE: If you wish to use a different hostname, e.g. `snerdly`, the
    following will work:

    ```bash
	$fab -H $IP change_hostname:snerdly
	```

(@) Wait for the machine to reboot. Configure WiFi:

	```bash
	# list which networks are visible to the Pi
	$ fab -H $IP iwlist

	# configure WiFi to connect to desired network
	# MYSSID is the SSID that you wish to connect to
	# MYPSK is the PSK for the SSID
	$ fab -H $IP configure_wifi --set ssid=MYSSID,psk=MYPSK
	```

	The Pi will reboot.

	You can remove the Pi's wired connection to the router.

(@) At this point, I used the router admin page to assign a static IP to the
    WiFi interface of the Pi.  This is optional.

	Note that the hostname of the Pi will be unknown to devices on the subnet
    unless your router includes a DNS server.  There are many ways to address
    this, but I chose just to stick with using the static IP for the time
    being. 

(@) Then configure the project directory on the	remote. 

	```bash
	$ fab -H $IP setup_projdir
	```

(@) Configure SSH on the remote machine.
	*IMPORTANT: this assumes that you have your SSH public key in 
	`~/.ssh/id_rsa` on the local system.*

	```bash
	$ fab -H $IP configure_ssh
	```

(@) Update and install software on remote machine.  This may take a while to
    run.

	```bash
	$ fab -H $IP update_software
	```

	Included in the packages that are installed is `libnss-mdns`.  This is a
    multicastDNS service.  It means that if other machines on your local
    network support mDNS, you will be able to contact the Pi using the
    hostname `remote.local`.

## Install the `remote` Software

With the base configuration complete, it is time to install the `remote`
application.

(@) Transfer all the `remote` software to the Pi using rsync:

	```bash
	$ cd $R
	$ make transfer
	```

	If you are developing on your local machine and make source changes, you
	simply run `make transfer` to ship the changed files over to the Pi.  Do this
	as often as you wish.

(@) The `remote` software runs on port 5000.  Setup an `iptables` rule to
    forward all traffic from port 80 to port 5000.

	```bash
	fab -H $IP forward_port_80
	```

## Configure LIRC

The LIRC software was installed earlier, but it was not configured.  We can
now configure it.

(@) Configure LIRC.  This configures pin 22 as GPIO `out` and
	pin 23 as  GPIO `in`.

	```bash
	$ fab -H $IP configure_lirc

	# check output of each for LIRC errors
	$ fab -H $IP dmesg
	$ fab -H $IP daemonlog

    ```

(@) Test LIRC Installation

	LIRC has been installed and should be running.  Here's a quick test, assuming
	that you have one of the remotes that are specified in lircd.conf.

	```bash
	$ irw
	```
	
	If `irw` runs and doesn't exit, so far so good.  At least the LIRC device can
	be opened.

	Point the remote at the IR receiver and press a few buttons.  Each press of a
	button that is configured in `lircd.conf` should cause `irw` to print the key
	name of that button.  If this happens, you are in great shape.

	If you don't have one of the configured remotes handy, but you do have an IR
	remote, you can run `irrecord` and record some key names and codes.

	```bash
	$ irrecord -d /dev/lirc0 -d /tmp/testconf.conf
	```

	At completion, `testconf.conf` should contain key names and codes.


## Final Configuration

With the `remote` application installed and LIRC running, there are a couple
of loose ends to take care of:

(@) Configure `systemd` so that it starts `remote` at boot:

    ```
	fab -H $IP start_at_boot
	```

(@) By default, `Flask` will run the a web server on port 5000.  Rather than
    fool around with the config to allow `Flask` to use port 80, forward all
    traffic on port 80 to port 5000 using `iptables`.  Create and install a
    `system` unit to re-enable the port forwarding during the boot process.

    ```
	fab -H $IP configure_port_80
	```


## Use `remote`

All the components should be working.  Time to use the `remote`
application. Point your browser at `$IP` and enjoy. 

`remote` is currently configured to use the internal `flask` webserver to
serve the site.  Since this is a single-user extremely low activity site, it
is probably sufficient.  I may at some point convert to Apache/wsgi.

Any errors, actually any output, from `flask` will appear in
`/var/log/daemon.log`.  As the application is configured now, every button
click on the UI will create a log entry.  If you want to monitor what's going
on closely, it's probably best to `ssh` to the Pi and then `tail -f
/var/log/daemon.log`.

## Helpful Resources

Here are a few resources that were extremely helpful in getting this up and
going:


http://serverfault.com/questions/112795/how-can-i-run-a-server-on-linux-on-port-80-as-a-normal-user

https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/chap-Managing_Services_with_systemd.html

http://www.drmoron.org/tv-remote/

