
function pushToPage(url) {
    $('#status').load(encodeURI(url));
}

function remote(url) {
    pushToPage(url);
    button_clicked();
}

function channelURL(url, channel) {
    /* TODO: check numbers only & length */
    $('input#channel').val('');
    return url + '/' + channel
}

$(document).ready(function() 
    { 
        $("#CHANNELS").tablesorter(); 
        $("#MUSIC").tablesorter(); 
        $("#FAVORITES").tablesorter(); 

	hoverIfDesktop();

    } 
); 

function button_unclicked (button) {
    $(button).removeClass('reverse');
}

function button_clicked () {
    var button = window.event.srcElement;

    $(button).addClass('reverse');

    setTimeout( 
	function () {
	    $(button).removeClass('reverse');
	    // button_unclicked(button);
	},
	200);
}

function scrollToTop() {
    $(window).scrollTop(0); // If you work with jQuery,
}

function info() {
    var msg = 'width='  + document.body.clientWidth + '\n' +
	      'height=' + document.body.clientHeight + '\n';
    alert(msg);
}


/* ********************************************************************************
 * I can't get '@media (hover:on-demand)' or  '@media (hover:hover)' to work.
 * Therefore, this workaround was created. See:
 * http://www.javascriptkit.com/dhtmltutors/cssmediaqueries4.shtml
 */
function mediaqueryresponse(mql){
    if (mql.matches){ // if media query matches
	console.log("The condition " + mql.media + " has been met")
	// is desktop/laptop
	$('.fa').addClass('desktop');
	$('a').addClass('desktop');
	$('.columnHeading').addClass('desktop');

	$('.fa').removeClass('touchscreen');
    }
    else{
	console.log("Condition not met yet")
	// is touchscreen
	$('.fa').addClass('touchscreen');

	$('.fa').removeClass('desktop');
	$('a').removeClass('desktop');
	$('.columnHeading').removeClass('desktop');
    }
}
/*
  | FEATURE         | TOUCHSCREEN | TOUCHSCREEN | DESK/LAPTOP | DESK/LAPTOP  |
  |                 |             | +MOUSE      |             | +TOUCHSCREEN |
  |                 |             |             |             |              |
  | hover:none      | false       | false       | false       | false        |
  | hover:on-demand | true        | true        | false       | false        |
  | hover:hover     | false       | false       | true        | true         |
 */
function hoverIfDesktop() {
    var mql = window.matchMedia("(hover:hover)");
    // addListener() only fires when the state of the window changes, which does not
    // occur when the page first loads.  Therefore mediaqueryresponse is called now
    // *and* passed to addListener().
    mediaqueryresponse(mql);              // call function explicitly at run time
    mql.addListener(mediaqueryresponse);  // attach function to listen in on state changes
}
/* ********************************************************************************/

