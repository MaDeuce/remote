'''
This program will create a list of all channels available on the
specified cable service.  This is a hack and only works with tvtv.us,
but it's better than typing everything in.

Because the website uses javascript to populate the content of the
directory listing, it isn't possible to get the data with simple
screen scraping.  To get around this problem, Selenium is used.
Install it via 'pip install selenium'.

If you aren't familiar with Selenium, don't be surprised when this
program opens a Firefox window.  When it appears, don't do anything to
it, or you may disturb the script.  Firefox will close when the script
completes.  If you don't have Firefox installed, you can use a
different webdriver.

The format is intended to generate the 'tv' dict used by listings.py.
I use emacs to edit listings.py and then run the shell command 'python
chanlist.py', inserting the output into the current buffer.

'''

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains


def tvtv(url):
    driver = webdriver.Firefox()
    driver.get(url)
    print 'tv = {'
    for row in driver.find_elements_by_class_name('tvm_td_chn'):
        channel = row.find_element_by_class_name('tvm_txt_chan_num').text
        name    = row.find_element_by_class_name('tvm_txt_chan_name').text
        name    = "'%s'" % name
        info    = "    %-15s: %3s," % (name, channel.rstrip())
        print info
    print '}'
    driver.close()

import pdb

def aol(url):
    ''' Get channel info from AOL.  More complete than tvtv? '''

    next_page = '''
    function nextPage() {
    
        var documentHeight = document.documentElement.scrollHeight;
        var windowHeight   = document.documentElement.clientHeight;
    
        window.scrollBy(0, windowHeight);

    }
    nextPage()
    '''

    def getimg(driver):
        '''Force the loading of img by scrolling through the 
           site, a page at a time, as needed.  Ugly, but it works.
        '''
        img = None
        count = 0

        while not img and count < 10:
            try:
                img = WebDriverWait(driver, 5).until(
                    lambda x: row.find_element_by_class_name('grid-img').get_attribute('src'))
            except TimeoutException:
                driver.execute_script(next_page)
                continue
            count += 1
        return img

    driver = webdriver.Firefox()
    driver.get(url)
    print 'tv = {'
    for row in driver.find_elements_by_class_name('grid-source'):
        channel = row.find_element_by_class_name('grid-channel').text
        name    = row.find_element_by_class_name('grid-network').text
        img     = "'%s'" % getimg(driver)
        name    = "'%s'" % name
        info    = "    %-15s: (%3s, %s)," % (name, channel, img)
        print info
    print '}'
    driver.close()



if __name__ == '__main__':

    # tvtv(url='http://www.tvtv.us/la/lake-charles/70601/lu90157')

    # AOL is a little slow, due to lazy image loading on their site.
    aol(url='http://tvlistings.aol.com/listings/la/lake-charles/suddenlink-communications/LA17421%7CX')
