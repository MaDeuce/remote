POPTS=markdown+fenced_code_blocks+fenced_code_attributes+example_lists

%.pdf: %.md
	pandoc -f $(POPTS) -s -o $@ $<

