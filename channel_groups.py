#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
ChannelGroups defines the cable channels that are subscribed to/available,
as well as music channels and a favorites list.

'''

class ChannelGroups(object):

    cable = {

        # these values for 'cable' were generated via 'python chanlist.py'
        'AETVHD'       : (778, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/51529.png', 'A&E Channel'),
        'AMCHD'        : (762, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'AMC'),
        'APLHD'        : (758, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/57394.png', 'Animal Planet'),
        'AXSTVSD'      :  (83, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'unknown'),
        'BBCAHD'       : (808, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/64492.png', 'BBC America'),
        'BLAZEHD'      : (741, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'Glen Beck'),
        'BRAVOHD'      : (761, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/58625.png', 'Bravo'),
        'CNBCHD'       : (760, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/58780.png', 'CNBC'),
        'CNNHD'        : (738, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/58646.png', 'CNN'),
        'CSPAN2'       :  (49, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/10162.png', 'CSPAN 2'),
        'CSPANHD'      : (870, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/68344.png', 'CSPAN'),
        'CST'          :  (57, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/31950.png', 'sports'),
        'DISNHD'       : (764, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/59684.png', 'Disney'),
        'DSCHD'        : (757, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/56905.png', 'Discovery Channel'),
        'EHD'          : (786, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/61812.png', 'E!'),
        'ELREY'        :  (77, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'unknown'),
        'ESPN2HD'      : (752, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/45507.png', 'ESPN 2'),
        'ESPNHD'       : (751, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/32645.png', 'ESPN'),
        'EWTN'         :  (16, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/10183.png', 'Catholic'),
        'FNCHD'        : (749, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/60179.png', 'Fox News Channel'),
        'FOODHD'       : (776, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/50747.png', 'Food Channel'),
        'FREFMHD'      : (765, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/59615.png', 'ABC Family'),
        'FS1HD'        : (734, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'unknown'),
        'FSNOHD'       : (750, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'unknown'),
        'FSSLA4'       : (134, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'unknown'),
        'FXHD'         : (753, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/58574.png', 'FX'),
        'FXXHD'        : (742, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/66379.png', 'Soccer'),
        'GOAC005'      :   (5, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'Gvmt. Access'),
        'GSNHD'        : (825, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'unknown'),
        'HALLHD'       : (783, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/66268.png', 'Hallmark'),
        'HGTVD'        : (775, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/49788.png', 'Home & Garden'),
        'HLNHD'        : (785, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/64549.png', 'NLN Ntwk'),
        'HMMHD'        : (869, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/46710.png', 'Hallmark'),
        'HSTRYHD'      : (779, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/57708.png', 'History Channel'),
        'IDHD'         : (748, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/65342.png', 'Investigation'),
        'INSP'         :  (19, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/11066.png', 'Inspiration'),
        'KATC'         :   (3, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/10332.png', 'KATC - ABC'),
        'KBMTDT'       : (720, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/31656.png', 'KBMT - ABC'),
        'KJAC'         :   (2, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/62398.png', 'KJAC - NBC'),
        'KFAMLP'       :  (20, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/32936.png', 'Religion'),
        'KFDM'         :   (6, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/15005.png', 'KFDM - CBS'),
        'KLFYDT'       : (710, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/20467.png', 'KLFY - CBS'),
        'KLTLDT'       : (700, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/61010.png', 'LPB'),
        'KLTLDT2'      : (701, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/62431.png', 'LPB 2'),
        'KLTLDT3'      : (702, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/44767.png', 'LPB 3'),
        'KPLCDT'       : (730, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/44334.png', 'KPLC - NBC'),
        'KPLCDT2'      : (245, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/50546.png', 'Bounce'),
        'KVHPDT'       : (705, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/35776.png', 'Fox'),
        'KVHPDT2'      :   (7, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/64232.png', 'The CW'),
        'LIFEHD'       : (763, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/60150.png', 'Lifetime'),
        'LMNHD'        : (784, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/55887.png', 'Lifetime'),
        'LOOR060'      :  (60, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'Local Access'),
        'MGMHD'        : (781, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/58530.png', 'unknown'),
        'MNBCHD'       : (739, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/64241.png', 'MSNBC'),
        'NBCSNHD'      : (732, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/48639.png', 'Versus Sprts'),
        'OWNHD'        : (789, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'OWN'),
        'OXYGNHD'      : (831, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'Oxygen'),
        'PIVOTHD'      : (746, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'Pivot'),
        'SEC'          :  (34, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'SEC'),
        'SECAHD'       : (801, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'SEC Alt'),
        'SECH'         : (800, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'SEC 3'),
        'SPROUTH'      : (744, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'Sprout'),
        'STZENAC'      : (305, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/14871.png', 'Encore Action'),
        'STZENBK'      : (304, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/14870.png', 'Encore Drama'),
        'STZENC'       : (300, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/10178.png', 'Encore'),
        'STZENCL'      : (302, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/14764.png', 'Encore Love'),
        'STZENSU'      : (303, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/14766.png', 'Encore Mystery'),
        'STZENWS'      : (301, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/14765.png', 'Encore Westerns'),
        'SUNDHD'       : (868, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/71280.png', 'Sundance'),
        'SYFYHD'       : (754, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/58623.png', 'Syfy'),
        'TBN'          :  (19, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/14767.png', 'Religion'),
        'TBSHD'        : (745, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/58515.png', 'TBS'),
        'TCMHD'        : (766, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/64312.png', 'Turner Classic Mv'),
        'TLCHD'        : (756, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/57391.png', 'Learning Channel'),
        'TNTHD'        : (740, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/42642.png', 'TNT'),
        'TOONHD'       : (743, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/60048.png', 'Cartoon Ntwk'),
        'TRAVHD'       : (759, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/59303.png', 'Travel Channel'),
        'TRUTVHD'      : (737, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/64490.png', 'truTV'),
        'TVONEHD'      : (865, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/61960.png', 'TV One'),
        'UPHD'         : (747, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'UP'),
        'USAHD'        : (755, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/58452.png', 'USA'),
        'VEL'          :  (82, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/31046.png', 'Velocity'),
        'WEATHHD'      : (864, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/58812.png', 'Weather Channel'),
        'WEHD'         : (832, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/59296.png', 'WE'),
        'WGNASD'       :  (79, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/91097.png', 'WGN'),
        'WORD'         : (219, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/70387.png', 'Religion'),

    }


    music = {

        'R&B Classics':    (909, '', ''),
        'R&B Soul':        (910, '', ''),
        'Rock':            (913, '', ''),
        'Alternative':     (915, '', ''),
        'Adult Alt.':      (916, '', ''),
        'Classic Rock':    (918, '', ''),
        'Soft Rock':       (919, '', ''),
        'Pop/Country':     (931, '', ''),
        'Country 1':       (932, '', ''),
        'Country 2':       (933, '', ''),
        'Country 3':       (934, '', ''),
        'Blues':           (946, '', ''),
        'Classical 1':     (949, '', ''),
        'Classical 2':     (950, '', ''),

    }


    favorite = {

        'TCMHD'        : (766, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/64312.png', 'Turner Classic Mv'),
        'STZENWS'      : (301, 'http://media.i.tv.s3.amazonaws.com/channels/46x35/14765.png', 'Encore Westerns'),

    }


    @staticmethod
    def channelInfo(channels):
        for id in sorted(channels.keys()):
            number, img, description = channels[id]
            yield (id, number, img, description)


    @staticmethod
    def favoriteChannels():
        '''
        Called by Jinja2 template in index.html to populate <tbl>
        with favorites list.
        '''
        return ChannelGroups.channelInfo(ChannelGroups.favorite)


    @staticmethod
    def musicChannels():
        '''
        Called by Jinja2 template in index.html to populate <tbl>
        with music channels
        '''
        return ChannelGroups.channelInfo(ChannelGroups.music)


    @staticmethod
    def cableChannels():
        '''
        Called by Jinja2 template in index.html to populate <tbl>
        with all cable channels.
        '''
        return ChannelGroups.channelInfo(ChannelGroups.cable)




if __name__ == '__main__':


    CG = ChannelGroups

    for listFunc in [CG.musicChannels, CG.favoriteChannels, CG.cableChannels]:
        for (id, number, img, description) in listFunc():
            print id, number, img, description
