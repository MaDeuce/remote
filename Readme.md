# `Remote` - The AV Remote Control

This cleverly named application is an extremely simply web-based Audio-Video
controller.  It was designed to be used by my 102 year-old father who was
having increasing difficulty using his typical overly-complicated multi-device
multi-remote-control AV system.

There will be a post on my site (http://east.fm) soon which covers much of the
application in more detail, as well as the hardware interface.  Until then,
are few highlights follow:

* Runs a Raspberry Pi 3 with Raspbian Jessie 2106-03-18.  Definitely overkill,
  but easy to develop on.

* Extreme parsimony in choosing functionality.  Only the things my father would
  use are included in the UI, so as to minimize clutter.  This is not the remote
  to end all remotes and control anything.

* DVD and VCR controls are not included in v1.0.  They will be added in a
  subsequent release.

* Very high contrast text and icons.  The colors are indeed high-vis and were
  chosen to mimic the colors on the low-vision keyboard he uses on his iMac.
  You probably won't like the color choices, but you won't have any problems
  seeing them.

* Large fonts and icons.

* Intended to function in either landscape or portrait on tablet with 800x1280
  resolution.

* Extremely easy to go from virgin Pi and git repository to fully functional
  AV remote control.  As close to 'turnkey' as possible.


The remainder of this document assumes the source for this application is
located in `$R`.

## Documentation

Documentation for the application is built via:
```
cd $R
make pdf
```

This will create a PDF version of this document in `$R/Readme.pdf`.  It will
also create `$R/docs/setup.pdf`.

`setup.pdf` contains very detailed documentation outlining all the steps
necessary to install Raspbian on the Pi, configure Raspbian, install this
application on the Pi, and configure this application.

## Briefly

Although it is covered in detail by `setup.pdf`, these next bullet points
cover the high points of the application and its deployment.

* Downloading and installing the chosen version of Raspbian is fully
  automated.  All that has to be typed are a few `make` commands.

* After an initial `ssh` into the Pi at first boot, the remaining installation
  and configuration is handled on the development machine through the use of
  Fabric.  This makes the developer's life easy.

  Because I expected this software to be used on a daily basis, it had to be
  easy to maintain.  E.g., if the Pi was damaged, I didn't want to have to
  start from scratch figuring out how to redeploy the application.

* Similarly, I wanted it to be easy to make changes and deploy them (and
  rollback too!).  Fabric was the key to all this. If you make code changes on
  the development machine, it only takes a single command (`make transfer`) to
  sync the changes to the Pi.

## Requirements

### Development Machine

* OS-X or other unix-like OS, including a development environment (i.e.,
  `make` is available).

* SD card reader

* Working Python environment with the following packages installed:

     * Fabric

     * pandoc

* On same subnet as the Pi

* `rsync` executable

### Deployment Target

* Raspberry Pi 3 (suspect any Pi would be fine)

* Raspbian Jessie

* 32GB SD card

* On same subnet as development machine

* Live internet connection (only during installation)

## Notes

This runs Flask's own webserver, which is fine for a small application on an
isolated network with one concurrent user.  This application isn't intended to
be exposed live to the internet; doing so will almost certainly be unpleasant.


