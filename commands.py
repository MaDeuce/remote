import re
import subprocess
from sys import platform as _platform


development_system = _platform == 'darwin'

class DeviceCommand(object):
    '''
    Takes string used to define commands and splits into components.
    '''


    def __init__(self, device_command):
        self.command = device_command
        _, self.name, self.group, self.command = device_command.split('/')


class RemoteCommand(object):
    '''
    Takes string used to define commands and splits into components.
    '''


    def __init__(self, remote_command):
        self.command = remote_command
        self.remote, self.command = remote_command.split(':')


class Device(object):
    '''
    This class maps a device name used within this software to a
    (usually) longer device name that must be present in
    /etc/lirc/lircd.conf.  Traditionally, the device names in
    lircd.conf are the names/models of IR remote controls.  In this
    software, the name of the device being controlled is used
    instead.

    The class also maintains a state for each device.  As the devices
    are not supervised, the state is just a guess, but it is better
    than nothing.
    '''

    _instances = dict()

    def __init__(self, name, remote, commands):
        '''
        Arguments:
           name (str)      - short name of the device
           remote (str)    - longer name of the device used in lircd.conf
                             to specify IR signals which control the device
           commands (list) - list of DeviceCommand strings
        '''

        self.__class__._instances[name] = self
        self.name = name
        self.remote = remote

        self.commands = dict()
        for device_command, remote_commands in commands.iteritems():
            self.commands[device_command] = remote_commands

        
    @classmethod
    def get(klass, name):
        return Device._instances[name]


    @classmethod
    def all(klass):
        return Device._instances.viewvalues()


    def send(self, device_command):
        prog = './irsend' if development_system else 'irsend'
        arg1 = 'SEND_ONCE'
        device = Device.get(DeviceCommand(device_command).name)
        arg2 = device.remote
        history = []

        m = re.search('.*/(?P<channel>[0-9]+)$', device_command)
        if not m:
            # standard command
            for remote_command in device.commands[device_command]:
                rc = RemoteCommand(remote_command)
                arg2 = Device.get(rc.remote).remote
                arg3 = rc.command
                args = [prog, arg1, arg2, arg3]
                output = subprocess.check_output(args)
                history.append([args, output])

        else:
            # non-standard command.  device_command is of the form
            # '/cbl/channel/NNN', where NNN is the channel number to tune to.
            # cbl is only device at the moment with tuner, so hardcode it
            channel = m.group('channel')
            for digit in channel:
                arg3 = 'KEY_%s' % digit
                args = [prog, arg1, arg2, arg3]
                output = subprocess.check_output(args)
                history.append([args, output])

            args = [prog, arg1, arg2, u'KEY_OK']
            output = subprocess.check_output(args)
            history.append([args, output])

        return history


class Controller(object):

    def __init__(self):

        audio_commands = {
            '/aud/volume/up'           : ['aud:KEY_VOLUMEUP'],
            '/aud/volume/down'         : ['aud:KEY_VOLUMEDOWN'],
            '/aud/volume/mute'         : ['aud:KEY_MUTE'],
            '/aud/power/toggle'        : ['aud:KEY_POWER'],
        }
        self.audio = Device('aud', 'STR-DE935', audio_commands)

        cable_commands = {
            '/cbl/channel/up'          : ['cbl:KEY_CHANNELUP'],
            '/cbl/channel/down'        : ['cbl:KEY_CHANNELDOWN'],
            #/cbl/channel/NNN            defined in Device.send
            '/cbl/guide/show'          : ['cbl:KEY_EPG'],
            '/cbl/guide/hide'          : ['cbl:KEY_EXIT'],
            '/cbl/guide/up'            : ['cbl:KEY_UP'],
            '/cbl/guide/down'          : ['cbl:KEY_DOWN'],
            '/cbl/guide/pageup'        : ['cbl:KEY_PAGEUP'],
            '/cbl/guide/pagedown'      : ['cbl:KEY_PAGEDOWN'],
            '/cbl/guide/select'        : ['cbl:KEY_OK'],
            '/cbl/power/toggle'        : ['cbl:KEY_POWER'],
        }
        self.cable = Device('cbl', 'RNG110', cable_commands)

        tv_commands = {
            '/tv/power/toggle'        : ['tv:KEY_POWER'],
        }
        self.tv = Device('tv', 'PDP-607CMX', tv_commands)


        sys_commands = {
            '/sys/power/toggle'        : ['tv:KEY_POWER', 'cbl:KEY_POWER', 'aud:KEY_POWER'],
        }
        self.sys = Device('sys', 'System', sys_commands)



if __name__ == '__main__':

    from pprint import pprint as pp
    import sys

    for name in ['aud', 'tv', 'cbl']:
        device = Device.get(name)
        print 'Device = %s' % device.name
        for device_command in device.commands:
            print ' '*5  + device_command
            print '          ', device.send(device_command)

    print 'Test direct channel tuning'
    print '     ', Device.get('cbl').send('/cbl/channel/246')

