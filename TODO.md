(@) Would like to drop font-awesome + skeleton and start using materialize.
    I think the icons are more consistent.  If I do that, no point in not
    using the rest of materialize, dropping skeleton.

(@) I think there's some overly complicated stuff in remote.css.  Maybe wait
    until converting to materialize before spending time on it, as a lot will
    change then anyway.

(@) Convert to using less / CodeKit.

(@) Music listing format is a little off.

(@) minify all the things.

(@) The current object model is quite poor and is going to be a problem going forward.

(@) something something favicon.


